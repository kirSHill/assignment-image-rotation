//
// Created by Кирилл Шишков on 06.01.2023.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_MESSAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_MESSAGE_H

#include <stdbool.h>

bool message_open(FILE** const f, char* const file, enum file_mode mode);

bool message_read(struct bmp_header* header, struct image* image, FILE* const f, char* const file);

bool message_rotate(struct image* old_image, struct image* new_image, char* const file);

bool message_write(struct bmp_header* header, struct image* const image, FILE* const f, char* const file);

bool message_close(FILE** const f, char* const file);

#endif //ASSIGNMENT_IMAGE_ROTATION_MESSAGE_H
