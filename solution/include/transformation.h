//
// Created by Кирилл Шишков on 06.01.2023.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_TRANSFORMATION_H
#define ASSIGNMENT_IMAGE_ROTATION_TRANSFORMATION_H
#include "image.h"
#include "inttypes.h"
#include <stdio.h>

struct image image_rotate(struct image* const old_image, uint8_t* result);

#endif //ASSIGNMENT_IMAGE_ROTATION_TRANSFORMATION_H
