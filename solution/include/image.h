//
// Created by kirSHill on 18.10.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#include "file.h"
#include "inttypes.h"
#include <stdio.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image image_initialize(struct image* image, int64_t width, int64_t height);

void image_free(struct image* image);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
