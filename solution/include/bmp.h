//
// Created by kirSHill on 18.10.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H
#include "image.h"
#include  <stdint.h>
#include <stdio.h>

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

enum bmp_status {
    READ_HEADER_OK,
    READ_HEADER_ERROR,
    WRITE_HEADER_OK,
    WRITE_HEADER_ERROR,
    READ_DATA_OK,
    READ_DATA_ERROR,
    WRITE_DATA_OK,
    WRITE_DATA_ERROR,
    READ_FILE_OK,
    READ_FILE_ERROR,
    WRITE_FILE_OK,
    WRITE_FILE_ERROR,
};

enum bmp_status file_read(struct bmp_header* header, struct image* image, FILE* f);

enum bmp_status file_write(struct bmp_header* header, struct image* const image, FILE* f);

enum bmp_status data_read(struct bmp_header* header, struct image* image, FILE* f);

enum bmp_status data_write(struct bmp_header* header, struct image* const image, FILE* f);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
