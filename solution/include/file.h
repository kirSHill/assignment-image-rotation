//
// Created by kirSHill on 18.10.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_H
#include "image.h"
#include <stdint.h>
#include <stdio.h>

//struct bmp_header;
//struct image;

enum file_mode {
    READ,
    WRITE,
};

enum file_status {
    OPEN_OK,
    OPEN_ERROR,
    CLOSE_OK,
    CLOSE_ERROR,
};

enum file_status file_open(FILE** const f,char* const file, enum file_mode mode);

enum file_status file_close(FILE** f);

#endif //ASSIGNMENT_IMAGE_ROTATION_FILE_H
