//
// Created by Кирилл Шишков on 06.01.2023.
//
#include "transformation.h"
#include <stdio.h>
#include <stdlib.h>

static struct pixel* get_pixel(const struct image* image, uint64_t row, uint64_t column) {
    return &image->data[image->width * row + column];
}

static void change_pixel(struct image* image, struct pixel pixel, size_t index) {
    image->data[index] = pixel;
}

// rotating an image 90 degrees anticlockwise
struct image image_rotate(struct image* const old_image, uint8_t* result) {
    struct image new_image;
    new_image = image_initialize(&new_image, (int64_t)old_image->height, (int64_t)old_image->width);
    *result = 1;
    for (uint32_t i = 0; i < old_image->height; i++) {
        for (uint32_t j = 0; j < old_image->width; j++) {
            size_t index = j * old_image->height + (old_image->height - 1 - i);
            change_pixel(&new_image, *get_pixel(old_image, i, j), index);
        }
    }
    if (new_image.data == NULL) {
        *result = 0;
    } else {
        *result = 1;
    }
    return new_image;
}
