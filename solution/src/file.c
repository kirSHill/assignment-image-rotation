//
// Created by kirSHill on 18.10.2022.
//

#include "file.h"
#include <stdio.h>

// opening the closed file
enum file_status file_open(FILE** const f,char* const file, enum file_mode mode) {
    if (mode == READ) {
        *f = fopen(file, "rb");
    } else {
        *f = fopen(file, "wb");
    }
    if (*f == NULL) {

        return OPEN_ERROR;
    } else {
        return OPEN_OK;
    }
}

// closing the opened file
enum file_status file_close(FILE** f) {
    if (fclose(*f)) {
        return CLOSE_ERROR;
    } else {
        return CLOSE_OK;
    }
}

