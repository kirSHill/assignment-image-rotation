//
// Created by kirSHill on 18.10.2022.
//

#include "bmp.h"
#include <stdio.h>

static uint8_t padding_calculate(uint64_t width) {
    return ( 4 - (( 3 * width ) % 4 )) % 4;
}

// reading a data from the file
enum bmp_status data_read(struct bmp_header* header, struct image* image, FILE* f) {
    int32_t count = 0;
    const uint8_t padding = padding_calculate(header->biWidth);
    for (uint32_t i = 0; i < header->biHeight; i++) {
        if (fread(image->data + i*header->biWidth,sizeof (struct pixel),header->biWidth,f) == 0) {
            count++;
        }
        if (fseek(f,(long)padding,SEEK_CUR) != 0) {
            count++;
        }
    }
    if (count != 0) {
        return READ_DATA_ERROR;
    } else {
        return READ_DATA_OK;
    }
}

// writing a new data to the file
enum bmp_status data_write(struct bmp_header* header, struct image* image, FILE* f) {
    const uint8_t padding = padding_calculate(header->biWidth);
    int32_t count = 0;
    const char nullity[3] = {0,0,0};
    for (uint32_t i = 0; i < header->biHeight; i++) {
        if (fwrite(image->data + i * header->biWidth, sizeof(struct pixel), header->biWidth, f) != image->width || fwrite(nullity, 1, padding, f) != padding) {
            count++;
        }
    }
    if (count != 0) {
        return WRITE_DATA_ERROR;
    } else {
        return WRITE_DATA_OK;
    }
}


// reading a file's header
static enum bmp_status header_read(struct bmp_header* header, size_t size, size_t nitems, FILE* f) {
    if (fread(header, size, nitems, f) != 1) {
        return READ_HEADER_ERROR;
    } else {
        return READ_HEADER_OK;
    }
}

// creating a header for the new file
static struct bmp_header header_create(struct image* image) {
    struct bmp_header header = {
    .bfType = 0x4D42,
    .biSize = 40,
    .biPlanes = 1,
    .bfileSize = image->height * image->width * sizeof(struct pixel) + image->height * padding_calculate(image->width),
    .biWidth = image->width,
    .biHeight = image->height,
    .biBitCount = 24,
    .bOffBits = sizeof(struct bmp_header) };
    return header;
}

// writing a header to the file
static enum bmp_status header_write(struct bmp_header* header, struct image* image, FILE* f) {
    *header = header_create(image);
    if (fwrite(header, sizeof(struct bmp_header), 1, f) != 1) {
        return WRITE_HEADER_ERROR;
    } else {
        return WRITE_HEADER_OK;
    }
}


// reading from the file
enum bmp_status file_read(struct bmp_header* header, struct image* image, FILE* f) {
    header_read(header, sizeof(struct bmp_header), 1, f);
    image_initialize(image, header->biWidth, header->biHeight);
    data_read(header, image, f);
    return READ_FILE_OK;
}

// writing to the file
enum bmp_status file_write(struct bmp_header* header, struct image* image, FILE* f) {
    header_write(header, image, f);
    data_write(header, image, f);
    return WRITE_FILE_OK;
}
