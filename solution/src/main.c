//
// Created by kirSHill on 18.10.2022.
//

#include "bmp.h"
#include "file.h"
#include "image.h"
#include "message.h"
#include "transformation.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {

    // initialization of all variables
    (void) argc;
    (void) argv; // supress 'unused parameters' warning
    if (argc != 3) {
        fprintf(stderr, "%s", "You have to enter 2 program arguments!");
        return 1;
    }
    char *file_in = argv[1];
    char *file_out = argv[2];
    FILE* file_input;
    FILE* file_output;
    struct bmp_header header;
    struct image old_image;
    struct image new_image;

    // opening files for reading and writing
    if (!message_open(&file_input, file_in, READ)) {
        return 1;
    }
    if (!message_open(&file_output, file_out, WRITE)) {
        return 2;
    }

    // reading a header and an image from the file
    if (!message_read(&header, &old_image,  file_input, file_in)) {
        return 3;
    }

    // rotating an image
//    image_initialize(&new_image, (int64_t)old_image.height, (int64_t)old_image.width);
    if (!message_rotate(&old_image, &new_image, file_in)) {
        return 4;
    }
    // writing a header and an image to the new file
    if (!message_write(&header, &new_image, file_output, file_out)) {
        return 5;
    }

    // closing both files
    if (!message_close(&file_input, file_in)) {
        return 6;
    }
    if (!message_close(&file_output, file_out)) {
        return 7;
    }
    image_free(&old_image);
    image_free(&new_image);
    return 0;
}


