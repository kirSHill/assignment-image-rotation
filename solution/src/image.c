//
// Created by kirSHill on 18.10.2022.
//

#include "image.h"
#include <stdio.h>
#include <stdlib.h>

// initializing an image
struct image image_initialize(struct image* image, int64_t width, int64_t height) {
    image->data = malloc(height * width * sizeof(struct pixel));
    image->width = width;
    image->height = height;
    return *image;
}

void image_free(struct image* image) {
    free(image->data);
}


