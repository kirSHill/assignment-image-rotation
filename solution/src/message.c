//
// Created by Кирилл Шишков on 06.01.2023.
//

#include "bmp.h"
#include "file.h"
#include "message.h"
#include "transformation.h"

const char* open_message [] = {
        [OPEN_OK] = "File was opened successfully!\n",
        [OPEN_ERROR] = "! File was not opened successfully!\n",
};

const char* read_message [] = {
        [READ_HEADER_OK] = "Header was read successfully!\n",
        [READ_HEADER_ERROR] = "! Header was not read successfully!\n",
        [READ_DATA_OK] = "Data was read successfully!\n",
        [READ_DATA_ERROR] = "! Data was not read successfully!\n",
        [READ_FILE_OK] = "File was read successfully!\n",
        [READ_FILE_ERROR] = "! File was not read successfully!\n",
};

const char* write_message [] = {
        [WRITE_HEADER_OK] = "Header was written successfully!\n",
        [WRITE_HEADER_ERROR] = "! Header was not written successfully!\n",
        [WRITE_DATA_OK] = "Data was written successfully!\n",
        [WRITE_DATA_ERROR] = "! Data was not written successfully!\n",
        [WRITE_FILE_OK] = "File was written successfully!\n",
        [WRITE_FILE_ERROR] = "! File was not written successfully!\n",
};

const char* rotate_message [] = {
        [true] = "File was rotated successfully!\n",
        [false] = "! File was not rotated successfully!\n",
};

const char* close_message [] = {
        [CLOSE_OK] = "File was closed successfully!\n",
        [CLOSE_ERROR] = "! File was not closed successfully!\n",
};

bool message_open(FILE** const f, char* const file, enum file_mode mode) {
    enum file_status result = file_open(f, file, mode);
    fprintf(stderr, "%s. %s",file, open_message[result]);
    if (result == OPEN_ERROR) {
        printf("File %s can't be opened!", file);
        return 0;
    } else {
        return 1;
    }
}

bool message_read(struct bmp_header* header, struct image* image, FILE* const f, char* const file) {
    enum bmp_status result = file_read(header, image, f);
    fprintf(stderr, "%s. %s",file, read_message[result]);
    if (result == READ_FILE_ERROR) {
        printf("File %s can't be read!", file);
        return 0;
    } else {
        return 1;
    }
}

bool message_rotate(struct image* old_image, struct image* new_image, char* const file) {
    uint8_t result;
    *new_image = image_rotate(old_image, &result);
    fprintf(stderr, "%s. %s",file, rotate_message[result]);
    if (result == 0) {
        printf("Image %s can't be rotated!", file);
        return 0;
    } else {
        return 1;
    }
}

bool message_write(struct bmp_header* header, struct image* const image, FILE* const f, char* const file) {
    enum bmp_status result = file_write(header, image, f);
    fprintf(stderr, "%s. %s",file, write_message[result]);
    if (result == WRITE_FILE_ERROR) {
        printf("File %s can't be written!", file);
        return 0;
    } else {
        return 1;
    }
}

bool message_close(FILE** const f, char* const file) {
    enum file_status result = file_close(f);
    fprintf(stderr, "%s. %s",file, close_message[result]);
    if (result == CLOSE_ERROR) {
        printf("File %s can't be closed!", file);
        return 0;
    } else {
        return 1;
    }
}
